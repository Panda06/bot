from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

example = InlineKeyboardButton(text="Посмотреть пример проблемы", callback_data='get_example')
another_example = InlineKeyboardButton(text="Еще вариант", callback_data='get_another_example')
yet_another_example = InlineKeyboardButton(text="Еще вариант", callback_data='get_yet_another_example')
male_ready = InlineKeyboardButton(text="Я готов начать", callback_data='male')
female_ready = InlineKeyboardButton(text="Я готова начать", callback_data='female')
have_idea = InlineKeyboardButton(text="Да, я знаю что делать!", callback_data="has_idea")
have_no_idea = InlineKeyboardButton(text="Нет, я пока не знаю что делать.", callback_data="has_no_idea")
have_one_idea = InlineKeyboardButton(text="У меня есть одна идея", callback_data="has_one_idea")
have_some_idea = InlineKeyboardButton(text="У меня есть несколько вариантов", callback_data="has_some_idea")
yes = InlineKeyboardButton(text="Да", callback_data='yes')
no = InlineKeyboardButton(text="Нет", callback_data='no')
next_question = InlineKeyboardButton(text="Следующий вопроc", callback_data="next")
want_to_end_this_shit = InlineKeyboardButton(text="Закончить разговор", callback_data="end")
talk_problem = InlineKeyboardButton(text="Разобраться с мыслями", callback_data='talk')
start = InlineKeyboardButton(text="Начать заново", callback_data='start')
need_more_questions = InlineKeyboardButton(text="Нет, хочу еще подумать", callback_data="next")
finish_conv = InlineKeyboardButton(text="Да, можно закончить разговор", callback_data="end")
explain = InlineKeyboardButton(text='Пояснение вопроса', callback_data='explain')
questions = InlineKeyboardButton(text='Показать вопросы', callback_data='questions')
reset_button = InlineKeyboardButton(text='Давай решим другой вопрос!', callback_data='reset')
stop_button = InlineKeyboardButton(text='Закончить разговор', callback_data='stop')
yes_button = InlineKeyboardButton(text='Да, можно', callback_data='yes')
no_button = InlineKeyboardButton(text='Нет, нельзя', callback_data='no')
effect = InlineKeyboardButton(text='Не знаю', callback_data='effect')
effects = InlineKeyboardButton(text='Не знаю', callback_data='effects')
new_task = InlineKeyboardButton(text='Да, давай', callback_data='yes')
cont = InlineKeyboardButton(text='Нет, хочу закончить решение этого вопроса', callback_data='no')
iss = InlineKeyboardButton(text='Нет, для меня важно решить эту задачу', callback_data='issue')
cont_talk = InlineKeyboardButton(text='Продолжить беседу', callback_data='cont_talk')
new = InlineKeyboardButton(text='Давай обсудим другой вопрос', callback_data='reset')
stop = InlineKeyboardButton(text='Хочу закончить разговор', callback_data='stop')
no_stop = InlineKeyboardButton(text='Нет, хочу закончить разговор', callback_data='stop')
new_issue = InlineKeyboardButton(text='Решить новую задачу', callback_data='issue')
want = InlineKeyboardButton(text='Хочу', callback_data='issue')
think_button = InlineKeyboardButton(text='Нет, нужно еще подумать', callback_data='think')
ideas_button = InlineKeyboardButton(text='Да, есть', callback_data='has')
no_ideas_button = InlineKeyboardButton(text='Нет, ни одного', callback_data='no_has')

yes_want = InlineKeyboardButton(text='Да, хочу', callback_data='yes')
no_want = InlineKeyboardButton(text='Нет, не хочу', callback_data='no')
kill_me = InlineKeyboardButton(text='Нет, нужно остановиться', callback_data='no')
not_kill_me = InlineKeyboardButton(text='Да, стоит продолжить', callback_data='yes')

kill_keyboard = InlineKeyboardMarkup()
kill_keyboard.add(not_kill_me)
kill_keyboard.add(kill_me)

want_keyboard = InlineKeyboardMarkup()
want_keyboard.add(yes_want)
want_keyboard.add(no_want)

satisfied_keyboard1 = InlineKeyboardMarkup()
satisfied_keyboard1.add(ideas_button)
satisfied_keyboard1.add(no_ideas_button)
satisfied_keyboard1.add(effects)



prelast_keyboard = InlineKeyboardMarkup()
prelast_keyboard.add(new_issue)
prelast_keyboard.add(stop_button)

last_keyboard = InlineKeyboardMarkup()
last_keyboard.add(want)
last_keyboard.add(no_stop)

reset_or_stop_keyboard = InlineKeyboardMarkup()
reset_or_stop_keyboard.add(new)
reset_or_stop_keyboard.add(stop)


think_keyboard = InlineKeyboardMarkup()
think_keyboard.add(think_button)
think_keyboard.add(ideas_button)


end_or_cont_keyboard = InlineKeyboardMarkup()
end_or_cont_keyboard.add(iss)
end_or_cont_keyboard.add(new)

cont_talk_keyboard = InlineKeyboardMarkup()
cont_talk_keyboard.add(cont_talk)

new_task_keyboard = InlineKeyboardMarkup()
new_task_keyboard.add(new_task)
new_task_keyboard.add(no_stop)

limited_keyboard = InlineKeyboardMarkup()
limited_keyboard.add(*[yes_button, no_button])

reasons_keyboard = InlineKeyboardMarkup()
reasons_keyboard.add(InlineKeyboardButton(text="Да, это так", callback_data='yes'))
reasons_keyboard.add(InlineKeyboardButton(text="Нет, у меня есть другие причины", callback_data='no'))

another_question_stop_keyboard = InlineKeyboardMarkup()
another_question_stop_keyboard.add(reset_button)
another_question_stop_keyboard.add(stop_button)


start_keyboard = InlineKeyboardMarkup()
start_keyboard.add(start)

example_keyboard = InlineKeyboardMarkup()
example_keyboard.add(example)
example_keyboard.add(*[male_ready, female_ready])


male_example_keyboard = InlineKeyboardMarkup()
male_example_keyboard.add(example)
male_example_keyboard.add(male_ready)

female_example_keyboard = InlineKeyboardMarkup()
female_example_keyboard.add(example)
female_example_keyboard.add(female_ready)

another_example_keyboard = InlineKeyboardMarkup()
another_example_keyboard.add(another_example)
another_example_keyboard.add(*[male_ready, female_ready])

male_another_example_keyboard = InlineKeyboardMarkup()
male_another_example_keyboard.add(another_example)
male_another_example_keyboard.add(male_ready)

female_another_example_keyboard = InlineKeyboardMarkup()
female_another_example_keyboard.add(another_example)
female_another_example_keyboard.add(female_ready)

yet_another_example_keyboard = InlineKeyboardMarkup()
yet_another_example_keyboard.add(yet_another_example)
yet_another_example_keyboard.add(*[male_ready, female_ready])

male_yet_another_example_keyboard = InlineKeyboardMarkup()
male_yet_another_example_keyboard.add(yet_another_example)
male_yet_another_example_keyboard.add(male_ready)

female_yet_another_example_keyboard = InlineKeyboardMarkup()
female_yet_another_example_keyboard.add(yet_another_example)
female_yet_another_example_keyboard.add(female_ready)

final_keyboard = InlineKeyboardMarkup()
final_keyboard.add(*[male_ready, female_ready])

male_final_keyboard = InlineKeyboardMarkup()
male_final_keyboard.add(male_ready)

female_final_keyboard = InlineKeyboardMarkup()
female_final_keyboard.add(female_ready)

idea_keyboard = InlineKeyboardMarkup()
idea_keyboard.add(have_idea)
idea_keyboard.add(have_no_idea)

talk_keyboard = InlineKeyboardMarkup()
talk_keyboard.add(have_one_idea)
talk_keyboard.add(have_some_idea)

situation_keyboard = InlineKeyboardMarkup()
situation_keyboard.add(*[yes, no])

next_or_end_keyboard = InlineKeyboardMarkup()
next_or_end_keyboard.add(next_question)
next_or_end_keyboard.add(want_to_end_this_shit)

ready_button = InlineKeyboardMarkup()
ready_button.add(InlineKeyboardButton(text="Готово!", callback_data="ready"))

talk_problem_keyboard = InlineKeyboardMarkup()
talk_problem_keyboard.add(want_to_end_this_shit)
talk_problem_keyboard.add(talk_problem)

next_or_finish_keyboard = InlineKeyboardMarkup()
next_or_finish_keyboard.add(need_more_questions)
next_or_finish_keyboard.add(finish_conv)

explain_keyboard = InlineKeyboardMarkup()
explain_keyboard.add(explain)

questions_keyboard = InlineKeyboardMarkup()
questions_keyboard.add(questions)

satisfied_keyboard = InlineKeyboardMarkup()
satisfied_keyboard.add(effect)
satisfied_keyboard.add(*[yes, no])
