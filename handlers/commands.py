import time
from hashlib import md5

from aiogram import types
from aiogram.dispatcher.filters import Command, Text
from aiogram.utils.emoji import emojize
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.types.input_file import InputFile
from pymongo import MongoClient

from handlers.misc import dp
from handlers.misc import bot
from handlers.states import StartStates, RootStates
from utils import get_name

BOT_NAME = "Валли"
client = MongoClient("127.0.0.1", 27017)
db = client['BOT_DB']
collection = db['bot_table']
users = db['users']


@dp.message_handler(commands=["start"], state="*")
async def welcome(msg, state):
    text = "Привет! Рад тебя видеть!"
    await bot.send_message(msg.from_user.id, text)
    user_id = md5(get_name(msg.from_user).encode()).hexdigest()
    user_find = users.find_one({'id': user_id})
    if user_find:
        async with state.proxy() as data:
            data['name'] = user_find['name']
            if user_find.get('gender') is not None:
                data['gender'] = user_find['gender']
        await want_you_talk(msg, state)
    else:
        time.sleep(0.5)
        text = "Давай знакомиться! Меня зовут Валли и я помогаю принимать решения. А как зовут тебя?"
        await bot.send_message(msg.from_user.id, text)
        await StartStates.name.set()


@dp.callback_query_handler(Text(equals=['start']), state="*")
async def restart_button(query, state):
    await welcome(query, state)


@dp.message_handler(commands=["stop"], state="*")
async def stop(msg, state):
    async with state.proxy() as data:
        data_dump = data.as_dict()
        collection.insert_one(data_dump)
    await state.finish()
    await bot.send_message(msg.from_user.id, "Опрос завершен")


@dp.message_handler(commands=["reset"], state="*")
async def reset(msg, state, dump=True):

    async with state.proxy() as data:
        if not data.get('name'):
            await welcome(msg, state)
            return
        if dump:
            data_dump = data.as_dict()
            collection.insert_one(data_dump)
        name = data['name']
        gender = data.get('gender', None)

    await state.finish()
    await StartStates.name.set()
    await state.update_data(name=name)
    if gender is not None:
        await state.update_data(gender=gender)
    await start(msg, state)


@dp.callback_query_handler(Text(equals=['reset', 'stop']))
async def stop_another(query, state):
    if query.data == 'reset':
        await reset(query, state)
    if query.data == 'stop':
        await stop(query, state)


from handlers.root_commands import *
from handlers.left_commands import *
from handlers.mental_gum_commands import *
from handlers.right_commands import *
