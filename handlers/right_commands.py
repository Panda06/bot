import time

from aiogram.dispatcher.filters import Command, Text
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.types.input_file import InputFile

from handlers.commands import dp, bot, collection, reset
from handlers import left_commands
from handlers.root_commands import end, stop, reset
from handlers.states import HasNoIdea, RootStates
from handlers.keyboards import idea_keyboard, explain_keyboard, explain, ready_button, questions_keyboard
from handlers.keyboards import situation_keyboard, cont_talk_keyboard, think_keyboard, new_task_keyboard
from handlers.keyboards import reset_or_stop_keyboard, end_or_cont_keyboard, another_question_stop_keyboard


@dp.callback_query_handler(Text(equals=['reset', 'stop']), state=HasNoIdea.wait9)
async def gunsnrose(query, state):
    if query.data == 'reset':
        await reset(query, state)
    elif query.data == 'stop':
        await stop(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasNoIdea.wait9)
async def oasis(query, state):
    if query.data == 'yes':
        text = 'Отлично, если ты согласен с последствиями своего решения и понимаешь, что несешь за них ответственность, то мы можем обсудить другой вопрос?'
        bot.send_message(query.from_user.id, text, reply_markup=reset_or_stop_keyboard)
    elif query.data == 'no':
        await function_name(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no', 'cont_talk']), state=HasNoIdea.wait8)
async def worked_with_sources(query, state):
    if query.data == 'yes':
        text = 'У тебя появились идеи того, как можно поступить в твоей ситуации?'
        await HasNoIdea.wait11.set()
        await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)
    elif query.data == 'no':
        async with state.proxy() as data:
            if data.get('hni_answr19'):
                text = 'В прошлый раз мы говорили про последствия твоего решения не собирать факты, ты согласен с ними?'
                await HasNoIdea.wait9.set()
                await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)
            else:
                text = 'А что произойдет в твоей жизни, если ты ничего не предпримешь?'
                await HasNoIdea.hni_answr16.set()
                await bot.send_message(query.from_user.id, text)
    elif query.data == 'cont_talk':
        await HasNoIdea.wait6.set()
        await continue_talk_(query, state)


@dp.message_handler(state=HasNoIdea.hni_answr19)
async def untitled_function(msg, state):
    await state.update_data(hni_answr19=msg.text)
    await HasNoIdea.wait8.set()
    text = 'Отлично! Теперь внеси эту задачу в календарь и нажми на кнопку "продолжить беседу", когда справишься с ней.'
    await msg.answer(text, reply_markup=cont_talk_keyboard)


async def function_name(query, state):
    text = 'В таком случае без дополнительных фактов о ситуации не обойтись.'
    await HasNoIdea.hni_answr19.set()
    await bot.send_message(query.from_user.id, text)
    async with state.proxy() as data:
        text = f'Сколько времени тебе нужно, чтобы поработать с этими источниками: {data["hni_answr14"]} ? Напиши дату и время, когда мы сможем продолжить разговор.'
        time.sleep(1)
        await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['reset', 'issue']), state=HasNoIdea.wait7)
async def the_rolling_stone(query, state):
    if query.data == 'reset':
        await reset(query, state)
    elif query.data == 'issue':
        await function_name(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasNoIdea.wait7)
async def inactivity(query, state):
    if query.data == 'yes':
        text = 'Отлично, если ты согласен с последствиями своего решения и понимаешь, что несешь за них ответственность, то мы можем обсудить другой вопрос?'
        await bot.send_message(query.from_user.id, text, reply_markup=end_or_cont_keyboard)
    elif query.data == 'no':
        await function_name(query, state)


@dp.message_handler(state=HasNoIdea.hni_answr18)
async def your_live_is_shit2(msg, state):
    await state.update_data(hni_answr18=msg.text)
    await HasNoIdea.wait7.set()
    text = 'Ты согласен с этими последствиями своего бездействия?'
    await msg.answer(text, reply_markup=situation_keyboard)


@dp.message_handler(state=HasNoIdea.hni_answr17)
async def your_live_is_shit2(msg, state):
    await state.update_data(hni_answr17=msg.text)
    await HasNoIdea.hni_answr18.set()
    text = 'Как ты себя будешь чувствовать, если ты будешь оттягивать принятие решения?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr16)
async def your_live_is_shit(msg, state):
    await state.update_data(hni_answr16=msg.text)
    await HasNoIdea.hni_answr17.set()
    text = 'Как твое решение не собирать факты отразится на людях, которые вовлечены в ситуацию?'
    await msg.answer(text)


@dp.callback_query_handler(Text(equals='ready'), state=HasNoIdea.wait11)
async def xxxtentacion(query, state):
    await end(query, state)


@dp.message_handler(state=HasNoIdea.hni_answr27)
async def dead_dolphins(msg, state):
    await HasNoIdea.wait11.set()
    await state.update_data(hni_answr27=msg.text)
    text = 'Отлично, благодаря тому, что ты ответил на мой вопрос, у тебя появилось понимание куда обратиться за источниками насмотренности.'
    await msg.answer(text)

    text = 'Выдели время в своем графике для работы с ними и внеси в календарь, а я пока подожду тебя тут.'
    time.sleep(1)
    await msg.answer(text, reply_markup=ready_button)


@dp.message_handler(state=HasNoIdea.hni_answr26)
async def all_good_things(msg, state):
    await HasNoIdea.hni_answr27.set()
    await state.update_data(hni_answr26=msg.text)
    text = 'Мы на финишной прямой! Это последнее задание.'
    await msg.answer(text)

    time.sleep(.5)
    text = 'Я предлагаю тебе применить принцип насмотренности к своей задаче. А именно: найти примеры того, как результат, к которому ты стремишься, был достигнут кем-то другим.'
    await msg.answer(text)

    time.sleep(1)
    text = 'Наш мозг устроен так, что он сам находит закономерности. Просто смотри - и ты увидишь нужные тебе подсказки.'
    await msg.answer(text)

    time.sleep(.7)
    text = 'Где, при помощи чего бы ты мог тренировать свою насмотренность?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr25)
async def diplo(msg, state):
    await HasNoIdea.hni_answr26.set()
    await state.update_data(hni_answr25=msg.text)
    text = 'Почему именно эта аналогия?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr24)
async def placebo(msg, state):
    await HasNoIdea.hni_answr25.set()
    await state.update_data(hni_answr24=msg.text)
    text = 'С чем ты мог бы сравнить свою задачу: с каким образом из природы, животного или растительного мира?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr23)
async def skillet(msg, state):
    await HasNoIdea.hni_answr24.set()
    await state.update_data(hni_answr23=msg.text)
    text = 'Какие из этих ресурсов у тебя уже есть?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr22)
async def bloodhound_gang(msg, state):
    await HasNoIdea.hni_answr23.set()
    await state.update_data(hni_answr22=msg.text)
    text = 'Какие ресурсы необходимы, чтобы достичь нужного результата?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr21)
async def red_hot_chili_peppers(msg, state):
    await HasNoIdea.hni_answr22.set()
    await state.update_data(hni_answr21=msg.text)
    text = 'Как изменилось твое понимание ситуации после предыдущего вопроса?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr20)
async def pink_floyd(msg, state):
    text = 'Подумай о том, кого ты уважаешь больше всего. Что бы он сделал, если бы столкнулся с подобной ситуацией?'
    await HasNoIdea.hni_answr21.set()
    await state.update_data(hni_answr20=msg.text)
    await msg.answer(text)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasNoIdea.wait10)
async def slipknot(query, state):
    if query.data == 'yes':
        await left_commands.answer_has_idea(query, state)
    elif query.data == 'no':
        text = 'Каким человеком ты хочешь выглядеть в этой ситуации?'
        await HasNoIdea.hni_answr20.set()
        await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasNoIdea.wait6)
async def worked_with_sources(query, state):
    if query.data == 'yes':
        text = 'У тебя появились идеи того, как можно поступить в твоей ситуации?'
        await HasNoIdea.wait10.set()
        await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)
    elif query.data == 'no':
        async with state.proxy() as data:
            if data.get('hni_answr16'):
                text = 'В прошлый раз мы говорили про последствия твоего решения не собирать факты, ты согласен с ними?'
                await HasNoIdea.wait9.set()
                await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)
            else:
                text = 'А что произойдет в твоей жизни, если ты ничего не предпримешь?'
                await HasNoIdea.hni_answr16.set()
                await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals='cont_talk'), state=HasNoIdea.wait6)
async def continue_talk_(query, state):
    text = 'Привет! Ты успел поработать с источниками?'
    await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)



@dp.message_handler(state=HasNoIdea.hni_answr15)
async def how_many_time(msg, state):
    await state.update_data(hni_answr15=msg.text)
    await HasNoIdea.wait6.set()
    text = 'Отлично! Теперь внеси эту задачу в календарь и нажми на кнопку "продолжить беседу", когда справишься с ней.'
    await msg.answer(text, reply_markup=cont_talk_keyboard)


@dp.message_handler(state=HasNoIdea.hni_answr14)
async def sources_(msg, state):
    await state.update_data(hni_answr14=msg.text)
    await HasNoIdea.hni_answr15.set()
    text = 'А сколько времени тебе нужно, чтобы поработать с ними? Напиши дату и время, когда мы сможем продолжить беседу.'
    await msg.answer(text)


@dp.callback_query_handler(Text(equals='ok'), state=HasNoIdea.wait55)
async def looked(query, state):
    text = 'Отлично! К каким 2-3 источникам ты обратишься?'
    await HasNoIdea.hni_answr14.set()
    await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['think', 'has']), state=HasNoIdea.wait5)
async def ideas_think(query, state):
    if query.data == 'has':
        await left_commands.answer_has_idea(query, state)
    elif query.data == 'think':
        await HasNoIdea.wait55.set()
        text = 'Если идей все еще нет, то стоит собрать дополнительные факты. Сейчас мы с тобой запланируем работу с открытыми источниками.'
        time.sleep(0.8)
        await bot.send_message(query.from_user.id, text)

        text = 'Они позволят тебе привнести новые факты в твое видение ситуации.  У меня для тебя есть подборка с вариантами источников. Посмотри!'
        time.sleep(1)
        await bot.send_message(query.from_user.id, text)
        button = InlineKeyboardMarkup()
        async with state.proxy() as data:
            text = "Посмотрел" if data.get('gender', True) else "Посмотрела"
            button.add(InlineKeyboardButton(text=text, callback_data="ok"))
            await bot.send_photo(query.from_user.id, InputFile("image.png"), reply_markup=button)


@dp.message_handler(state=HasNoIdea.hni_answr13)
async def impact_people(msg, state):
    await state.update_data(hni_answr13=msg.text)
    await HasNoIdea.wait5.set()
    text = 'Учитывая все собранные тобой факты и разные ракурсы , с которых  мы посмотрели на задачу, у тебя появились идеи того, как можно поступить в твоей ситуации?'
    await msg.answer(text, reply_markup=think_keyboard)


@dp.message_handler(state=HasNoIdea.hni_answr12)
async def impact_people(msg, state):
    await state.update_data(hni_answr12=msg.text)
    await HasNoIdea.hni_answr13.set()
    text = 'Как твое потенциальное решение может повлиять на этих людей?'
    await msg.answer(text)


async def which_people(query, state):
    text = 'Какие люди приходят тебе в голову, когда ты  думаешь о ситуации, которую мы сейчас решаем?'
    await HasNoIdea.hni_answr12.set()
    await bot.send_message(query.from_user.id, text)


@dp.message_handler(state=HasNoIdea.hni_answr11)
async def what_help_you(msg, state):
    await state.update_data(hni_answr11=msg.text)
    await which_people(msg, state)


@dp.message_handler(state=HasNoIdea.hni_answr10)
async def what_help_you(msg, state):
    await state.update_data(hni_answr10=msg.text)
    await HasNoIdea.hni_answr11.set()
    text = 'Что тебе помогло принять такое решение?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr9)
async def another_function1(msg, state):
    await state.update_data(hni_answr9=msg.text)
    await which_people(msg, state)


@dp.message_handler(state=HasNoIdea.hni_answr8)
async def another_function(msg, state):
    await state.update_data(hni_answr8=msg.text)
    await HasNoIdea.hni_answr9.set()
    text = 'Как ты думаешь, что тебе помешало принять решение, результатом которого ты был бы доволен?'
    await msg.answer(text)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasNoIdea.wait4)
async def details(query, state):
    if query.data == 'yes':
        await HasNoIdea.hni_answr10.set()
    elif query.data == 'no':
        await HasNoIdea.hni_answr8.set()
    text = 'Расскажи о деталях. Что это была за ситуация?'
    await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasNoIdea.wait3)
async def answer_has_sit(query, state):
    if query.data == 'yes':
        await HasNoIdea.wait4.set()
        text = 'Удовлетворил ли тебя результат и условия принятого решения в той ситуации?'
        await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)
    elif query.data == 'no':
        await which_people(query, state)


@dp.message_handler(state=HasNoIdea.hni_answr7)
async def have_sit(msg, state):
    await HasNoIdea.wait3.set()
    await state.update_data(hni_answr7=msg.text)
    text = 'А тебе когда-нибудь приходилось принимать подобное решение?'
    await msg.answer(text, reply_markup=situation_keyboard)


@dp.message_handler(state=HasNoIdea.hni_answr6)
async def why_now(msg, state):
    await HasNoIdea.hni_answr7.set()
    await state.update_data(hni_answr6=msg.text)
    text = 'Расскажи, почему решить вопрос нельзя прямо сейчас?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr6)
async def why_now(msg, state):
    await HasNoIdea.hni_answr7.set()
    await state.update_data(hni_answr6=msg.text)
    text = 'Расскажи, почему решить вопрос нельзя прямо сейчас?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr5)
async def relize(msg, state):
    await HasNoIdea.hni_answr6.set()
    await state.update_data(hni_answr5=msg.text)
    text = 'А что объективно изменится в реальности, если этот сценарий будет реализован?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr4)
async def other_peoples(msg, state):
    await state.update_data(hni_answr4=msg.text)
    await HasNoIdea.hni_answr5.set()
    text = 'Как обстоятельства этого результата будут влиять на других людей?'
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr3)
async def what_feel(msg, state):
    await state.update_data(hni_answr3=msg.text)
    await HasNoIdea.hni_answr4.set()
    text = 'Что ты будешь чувствовать (какие эмоции будешь испытывать, в каком состоянии находиться), если этот результат будет достигнут?'
    await msg.answer(text)


async def which_res(query, state):
    text = 'Каким результатом ты будешь доволен в связи с этим?'
    await HasNoIdea.hni_answr3.set()
    await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['reset', 'stop']), state=HasNoIdea.wait12)
async def the_who(query, state):
    if query.data == 'reset':
        await reset(query, state)
    elif query.data == 'stop':
        await stop(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasNoIdea.wait12)
async def sonic_youth(query, state):
    if query.data == 'no':
        text = 'В таком случае без сбора фактов о ситуации не обойтись.'
        await bot.send_message(query.from_user.id, text)
        time.sleep(1)
        async with state.proxy() as data:
            text = f'Сколько времени тебе нужно, чтобы найти ответы на эти вопросы {data["hni_answr1"]}? (Напиши дату и время в формате дд.мм.гггг в 00:00)'
            await bot.send_message(query.from_user.id, text)
            await HasNoIdea.hni_answr31.set()
    elif query.data == 'yes':
        text = 'Отлично, если ты согласен с последствиями своего решения и понимаешь, что несешь за них ответственность, то мы можем обсудить другой вопрос?'
        await bot.send_message(query.from_user.id, text, reply_markup=another_question_stop_keyboard)


@dp.message_handler(state=HasNoIdea.hni_answr31)
async def blake_shelton(msg, state):
    await state.update_data(hni_answr31=msg.text)
    await HasNoIdea.wait13.set()
    text = 'Отлично! Теперь внеси эту задачу в календарь и нажми на кнопку "продолжить беседу", когда справишься с ней.'
    await msg.answer(text, reply_markup=cont_talk_keyboard)


@dp.callback_query_handler(Text(equals=['yes', 'no', 'cont_talk']), state=HasNoIdea.wait13)
async def northern_lights(query, state):
    if query.data == 'yes':
        text = 'Отлично, если ты согласен с последствиями своего решения и понимаешь, что несешь за них ответственность, то мы можем обсудить другой вопрос?'
        await bot.send_message(query.from_user.id, text, reply_markup=new_task_keyboard)
    elif query.data == 'no':
        text = 'В таком случае без сбора фактов о ситуации не обойтись.'
        await bot.send_message(query.from_user.id, text)
        time.sleep(1)
        async with state.proxy() as data:
            text = f'Сколько времени тебе нужно, чтобы найти ответы на эти вопросы {data["hni_answr1"]}? (Напиши дату и время в формате дд.мм.гггг в 00:00)'
            await bot.send_message(query.from_user.id, text)
            await HasNoIdea.hni_answr31.set()
    elif query.data == 'cont_talk':
        await HasNoIdea.wait2.set()
        await continue_talk(query, state)

@dp.message_handler(state=HasNoIdea.hni_answr30)
async def black_sabbath(msg, state):
    text = 'Ты согласен с этими последствиями своего бездействия?'
    await state.update_data(hni_answr30=msg.text)
    await HasNoIdea.wait13.set()
    await msg.answer(text, reply_markup=situation_keyboard)


@dp.message_handler(state=HasNoIdea.hni_answr29)
async def jethro_tull(msg, state):
    text = 'Как ты себя будешь чувствовать (какие эмоции будешь испытывать, в каком состоянии находиться), если ты будешь оттягивать принятие решения?'
    await HasNoIdea.hni_answr30.set()
    await state.update_data(hni_answr29=msg.text)
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr28)
async def heaven_shall_burn(msg, state):
    await state.update_data(hni_answr28=msg.text)
    text = 'Как твое решение не собирать факты отразится на людях, которые вовлечены в ситуацию?'
    await HasNoIdea.hni_answr29.set()
    await msg.answer(text)


@dp.callback_query_handler(Text(equals='no'), state=HasNoIdea.wait2)
async def no_find_answers(query, state):
    async with state.proxy() as data:
        if data.get('hni_answr28'):
            text = 'В прошлый раз мы говорили про последствия твоего решения не собирать факты, ты согласен с ними?'
            await HasNoIdea.wait12.set()
            await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)
        else:
            text = 'А что произойдет в твоей жизни, если ты ничего не предпримешь?'
            await HasNoIdea.hni_answr28.set()
            await bot.send_message(query.from_user.id, text)


@dp.message_handler(state=HasNoIdea.hni_answr33)
async def heilung(msg, state):
    await state.update_data(hni_answr33=msg.text)
    await find_answers(msg, state)


@dp.callback_query_handler(Text(equals='questions'), state=HasNoIdea.hni_answr33)
async def louna(query, state):
    text = '1) Какие закономерности (правила, инструкции, принятые методы работы, и т.д.) существуют в этой сфере? (Например, законы малой социальной группы, если речь про конфликты в команде, или основные бизнес-метрики, если речь про увеличение продаж)\n2) Зачем нужно решать этот вопрос?\n3) Кому это нужно?\n4) Чьи интересы удовлетворит разрешение этой ситуации?\n5) Как твое решение отразится на задаче, которую ты решаешь в этой ситуации?'
    await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals='yes'), state=HasNoIdea.wait2)
async def zz_top(query, state):
    text = 'Отлично! Теперь запиши ответы на каждый из вопросов.'
    await HasNoIdea.hni_answr33.set()
    await bot.send_message(query.from_user.id, text, reply_markup=questions_keyboard)


async def find_answers(query, state):
    text = 'Раз со сбором фактов мы разобрались, давай перейдем к образу результата.'
    time.sleep(0.7)
    await bot.send_message(query.from_user.id, text)
    async with state.proxy() as data:
        text = f'До этого мы выясняли, что тебе нужно решить этот вопрос, чтобы {data["rs_answr3"]}'
        time.sleep(1)
        await bot.send_message(query.from_user.id, text)
    await which_res(query, state)


@dp.callback_query_handler(Text(equals='cont_talk'), state=HasNoIdea.wait2)
async def continue_talk(query, state):
    text = 'Привет! Ты успел найти ответы на вопросы, которые мы обсуждали в прошлый раз?'
    await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)


@dp.message_handler(state=HasNoIdea.hni_answr2)
async def get_time(msg, state):
    await state.update_data(hni_answr2=msg.text)
    await HasNoIdea.wait2.set()
    text = 'Отлично! Теперь внеси эту задачу в календарь и нажми на кнопку "продолжить беседу", когда справишься с ней.'
    time.sleep(0.6)
    await msg.answer(text, reply_markup=cont_talk_keyboard)


@dp.message_handler(state=HasNoIdea.hni_answr1)
async def question_numbers(msg, state):
    await HasNoIdea.hni_answr2.set()
    await state.update_data(hni_answr1=msg.text)
    text = 'Давай мы договоримся встретиться здесь после того, как ты найдешь ответы на эти вопросы.'
    time.sleep(0.6)
    await msg.answer(text)
    text = 'Когда ты сможешь продолжить нашу беседу?\nНапиши дату и время в формате дд.мм. гггг в 00:00'
    time.sleep(0.7)
    await msg.answer(text)


@dp.message_handler(state=HasNoIdea.hni_answr32)
async def muse(msg, state):
    await state.update_data(hni_answr32=msg.text)
    await find_answers(msg, state)


@dp.callback_query_handler(Text(equals=['questions']), state=HasNoIdea.hni_answr32)
async def glenn_gould(query, state):
    text = '1) Какие закономерности (правила, инструкции, принятые методы работы, и т.д.) существуют в этой сфере? (Например, законы малой социальной группы, если речь про конфликты в команде, или основные бизнес-метрики, если речь про увеличение продаж)\n2) Зачем нужно решать этот вопрос?\n3) Кому это нужно?\n4) Чьи интересы удовлетворит разрешение этой ситуации?\n5) Как твое решение отразится на задаче, которую ты решаешь в этой ситуации?'
    await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasNoIdea.wait1)
async def has_answers(query, state):
    if query.data == 'yes':
        await HasNoIdea.hni_answr32.set()
        text = 'Супер! Ты отлично подготовился к нашей беседе.'
        await bot.send_message(query.from_user.id, text)
        time.sleep(1)
        text = 'Запиши ответы на каждый из вопросов.'
        await bot.send_message(query.from_user.id, text, reply_markup=questions_keyboard)
    elif query.data == 'no':
        text = 'На какие из перечисленных вопросов у тебя еще нет ответов? (запиши номера вопросов)'
        await HasNoIdea.hni_answr1.set()
        await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals='ready'), state=HasNoIdea.wait1)
async def question_answers(query, state):
    text = 'У тебя есть ответы на все эти вопросы?'
    await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)


@dp.callback_query_handler(Text(equals='explain'), state=HasNoIdea.wait1)
async def explain_problems(query, state):
    keyboard = InlineKeyboardMarkup()
    async with state.proxy() as data:
        ready = InlineKeyboardButton(text=f'Посмотрел{"" if data["gender"] else "а"}!', callback_data='ready')
        keyboard.add(ready)
        text = 'Найди ответы на следующие вопросы:\n1) Какие закономерности (правила, инструкции, принятые методы работы, и т.д.) существуют в этой сфере? (Например, законы малой социальной группы, если речь про конфликты в команде, или основные бизнес-метрики, если речь про увеличение продаж) \n2) Зачем нужно решать этот вопрос? \n3) Кому это нужно?\n4) Чьи интересы удовлетворит разрешение этой ситуации?\n5) Как твое решение отразится на задаче, которую ты решаешь в этой ситуации?'
        time.sleep(0.6)
        await bot.send_message(query.from_user.id, text, reply_markup=keyboard)


@dp.callback_query_handler(Text(equals=['has_no_idea']), state=RootStates.wait4)
async def answer_has_no_idea(query, state):
    await HasNoIdea.wait1.set()
    await bot.send_message(query.from_user.id, "Давай подумаем что можно сделать!")
    text = 'Обычно, когда мы оказываемся в тупике и не знаем как поступить, нам банально не хватает фактов о ситуации.'
    time.sleep(0.6)
    await bot.send_message(query.from_user.id, text)
    time.sleep(1)
    text = 'Я подготовил для тебя памятку с вопросами, на которые стоит знать ответ перед решением проблемы. Посмотри.'
    keyboard = InlineKeyboardMarkup()
    look = InlineKeyboardButton(text='Посмотреть памятку', callback_data='explain')
    async with state.proxy() as data:
        ready = InlineKeyboardButton(text=f'Посмотрел{"" if data["gender"] else "а"}!', callback_data='ready')
        keyboard.add(look)
        keyboard.add(ready)
        await bot.send_message(query.from_user.id, text, reply_markup=keyboard)
