from aiogram import Bot, Dispatcher
from aiogram.bot import api
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from config import TOKEN, PATCHED_URL

setattr(api, "API_URL", PATCHED_URL)

# storage = RethinkDBStorage(db='bot_storage', table='storage')
storage = MemoryStorage()
bot = Bot(token=TOKEN)
dp = Dispatcher(bot, storage=storage)
