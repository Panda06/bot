import time
from hashlib import md5

from aiogram.dispatcher.filters import Command, Text
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.emoji import emojize

from handlers.commands import dp, bot, collection, stop, reset, users
from handlers.states import StartStates, RootStates, RootStatesRight, MentalGumStates
from handlers.keyboards import example_keyboard, situation_keyboard, explain_keyboard, talk_problem_keyboard
from handlers.keyboards import another_question_stop_keyboard, reasons_keyboard, limited_keyboard, idea_keyboard
from handlers.keyboards import another_example_keyboard, yet_another_example_keyboard, kill_keyboard, male_example_keyboard, female_example_keyboard
from handlers.keyboards import prelast_keyboard, last_keyboard, final_keyboard, cont_talk_keyboard, want_keyboard
from handlers.keyboards import male_another_example_keyboard, female_another_example_keyboard
from handlers.keyboards import male_yet_another_example_keyboard, female_yet_another_example_keyboard


from utils import get_name

@dp.callback_query_handler(Text(equals=['issue', 'stop']), state=RootStates.wait6)
async def system_of_a_down(query, state):
    if query.data == 'issue':
        await reset(query, state)
    elif query.data == 'stop':
        await stop(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=RootStates.wait6)
async def welshly_arms(query, state):
    if query.data == 'yes':
        text = 'Отлично, я рад, что был причастен к этому решению. Может быть ты хочешь решить новую задачу?'
        await bot.send_message(query.from_user.id, text, reply_markup=last_keyboard)
    elif query.data == 'no':
        text = 'Не унывай, мы можем заново подумать над этой задачей с учетом новых фактов. Хочешь?'
        await bot.send_message(query.from_user.id, text, reply_markup=last_keyboard)


@dp.message_handler(state=RootStates.rs_answr14)
async def nirvana(msg, state):
    text = 'Ты доволен своим результатом?'
    await state.update_data(rs_answr14=msg.text)
    await RootStates.wait6.set()
    await msg.answer(text, reply_markup=situation_keyboard)


@dp.message_handler(state=RootStates.rs_answr13)
async def fame_on_fire(msg, state):
    text = 'А что объективно изменилось в твоей жизни после принятого решения?'
    await state.update_data(rs_answr13=msg.text)
    await RootStates.rs_answr14.set()
    await msg.answer(text)


@dp.message_handler(state=RootStates.rs_answr12)
async def panic_at_the_disco(msg, state):
    text = 'Как твое решение  повлияло на других людей?'
    await state.update_data(rs_answr12=msg.text)
    await RootStates.rs_answr13.set()
    await msg.answer(text)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=RootStates.wait5)
async def sum41(query, state):
    if query.data == 'no':
        text = 'Давай попробуем воспринять твое нежелание принимать решение, как решение.'
        await bot.send_message(query.from_user.id, text)
        time.sleep(0.8)
    text = 'Как ты себя чувствуешь (какие эмоции испытываешь) в связи с принятым решением?'
    await RootStates.rs_answr12.set()
    await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals='cont_talk'), state=RootStates.wait5)
async def eminem(query, state):
    text = 'Привет! Время рассказывать про результаты. Тебе удалось принять решение?'
    await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)


async def get_resume(data):
    resume = 'За время нашей беседы мы:\n- Сформулировали вопрос, который будем решать: rs_answr6\n- Определили наши мотивы в ситуации: rs_answr3\n- Собрали относящиеся к ситуации факты: hni_answr33\n- Собрали относящиеся к ситуации факты: hni_answr32\n- Сформулировали вариант решения проблемы: hoi_answr1\n- Сформулировали вариант решения проблемы: hsi_answr1\n- Поняли какой вариант решения проблемы лучше всего удовлетворяет наши интересы: hsi_answr2\n- Разбили нашу ситуацию на несколько мелких задач: hi_answr7\n- Определились с шагом, который можно сделать в ближайшее время: hi_answr9\n- Обратились к дополнительным источникам информации: hni_answr14\n- Обратились к дополнительным источникам информации: hi_answr11\n- Определили источники насмотренности, которые могут вдохновить вас на решение: hni_answr27\n- А так же договорились, что ты решишь вопрос к : rs_answr11'
    keys = ['rs_answr6', 'rs_answr3', 'hoi_answr1', 'hsi_answr1', 'hsi_answr2', 'hi_answr7',
            'hi_answr9', 'hni_answr14', 'hi_answr11', 'hni_answr27', 'rs_answr11', 'hni_answr32', 'hni_answr33']
    for key in keys:
        if data.get(key):
            resume = resume.replace(key, data[key])

    return "\n".join([line for line in resume.split('\n') if 'answr' not in line])


@dp.message_handler(state=RootStates.rs_answr11)
async def nf(msg, state):
    await RootStates.wait5.set()
    await state.update_data(rs_answr11=msg.text)
    text = 'Отлично! Теперь внеси эту задачу в календарь и нажми на кнопку "продолжить беседу", когда справишься с ней.'
    await msg.answer(text)
    time.sleep(0.6)
    text = 'А пока посмотри резюме беседы, который я подготовил:'
    await msg.answer(text)
    async with state.proxy() as data:
        text = await get_resume(data)
        time.sleep(0.5)
        await msg.answer(text, reply_markup=cont_talk_keyboard)


@dp.message_handler(state=RootStates.rs_answr10)
async def thank_you(msg, state):
    await state.update_data(rs_answr10=msg.text)
    async with state.proxy() as data:
        text = f"{data['name']}, спасибо тебе большое за обратную связь!"
        time.sleep(0.5)
        await msg.answer(emojize(text))
        data_dump = data.as_dict()
        collection.insert_one(data_dump)
    text = 'Чтобы твой мозг занимался решением задачи эффективнее, стоит обозначить дедлайн.'
    await RootStates.rs_answr11.set()
    time.sleep(0.5)
    await msg.answer(text)
    text = 'К какому сроку ты планируешь решить этот вопрос? Напиши дату и время в формате дд.мм.гггг в чч:мм.'
    time.sleep(0.9)
    await msg.answer(text)
    # await state.finish()


@dp.message_handler(state=RootStates.rs_answr9)
async def liked_two(msg, state):
    await state.update_data(rs_answr9=msg.text)
    await RootStates.rs_answr10.set()
    text = "А что тебе понравилось в работе со мной?"
    await msg.answer(text)


@dp.message_handler(state=RootStates.rs_answr8)
async def help_you(msg, state):
    await state.update_data(rs_answr8=msg.text)
    await RootStates.rs_answr9.set()
    async with state.proxy() as data:
        text = f"Перед тем, как мы завершим разговор, и я дам тебе финальные рекомендации, скажи что бы ты посоветовал{'' if data.get('gender', True) else 'а'} улучшить во мне?"
        await msg.answer(text)


@dp.message_handler(state=RootStatesRight.rsr_answr1)
async def liked(msg, state):
    await state.update_data(rsr_answr1=msg.text)
    await RootStates.rs_answr10.set()
    text = "Перед тем, как мы завершим разговор, и я дам тебе финальные рекомендации, скажи, что тебе понравилось в работе со мной?"
    await msg.answer(text)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=RootStates.means)
async def means(query, state):
    if query.data == "yes":
        await RootStates.rs_answr8.set()
        text = "Я рад, что оказался полезен. Расскажи, что именно изменилось в мыслях?"
        await bot.send_message(query.from_user.id, text)
    else:
        await RootStatesRight.rsr_answr1.set()
        text = "Жаль, как думаешь, чего не хватило?"
        await bot.send_message(query.from_user.id, text)


async def end1(msg, state):
    await RootStates.means.set()
    text = "Расскажи, изменилось ли что-то в твоих мыслях после нашего разговора?"
    time.sleep(0.6)
    await bot.send_message(msg.from_user.id, text, reply_markup=situation_keyboard)


async def end(msg, state):
    async with state.proxy() as data:
        text = f"Ура! Мы финишировали. Даже если ты не приш{'ел' if data.get('gender', True) else 'ла'} к решению прямо сейчас, твой мозг озадачился и еще какое-то время сам будет искать ответ."
        await bot.send_message(msg.from_user.id, text)
        time.sleep(0.7)
    await end1(msg, state)


@dp.callback_query_handler(Text(equals=['reset', 'stop']), state=RootStates.wait7)
async def little_big(query, state):
    if query.data == 'reset':
        await reset(query, state)
    elif query.data == 'stop':
        await end1(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=RootStates.rs_answr7)
async def finish_shit(query, state):
    if query.data == 'yes':
        await RootStates.rs_answr6.set()
        text = 'Отлично! Ты определился со своими реальными мотивами и понял, что эта ситуация стоит того, чтобы быть обдуманной.'
        time.sleep(0.6)
        await bot.send_message(query.from_user.id, text)
        text = 'Как ты теперь  сформулируешь для себя вопрос, который мы будем решать? Чего мы хотим добиться?'
        time.sleep(0.5)
        await bot.send_message(query.from_user.id, text, reply_markup=explain_keyboard)

    elif query.data == 'no':
        await RootStates.wait7.set()
        text = 'Я очень рад, что помог тебе определить приоритетность этого вопроса. Теперь у тебя появился ресурс, чтобы решать более насущные задачи.'
        await bot.send_message(query.from_user.id, text, reply_markup=another_question_stop_keyboard)


@dp.message_handler(state=RootStates.rs_answr6)
async def has_you_idea(msg, state):
    await state.update_data(rs_answr6=msg.text)
    await RootStates.wait4.set()
    text = 'У тебя уже есть предположения, как можно поступить в этой ситуации?'
    await msg.answer(text, reply_markup=idea_keyboard)


@dp.callback_query_handler(Text(equals='explain'), state=RootStates.rs_answr6)
async def explain_question6(query, state):
    text = 'Возможно, мое размышление поступать или не поступать в MBA, связано не с желанием повысить свою квалификацию, как я это формулирую, а с желанием, чтобы на работе больше ценили. С такой формулировкой нужно будет понять какие в принципе есть возможности для повышения своей ценности на работе. Возможно, MBA здесь ни при чем.'
    time.sleep(1)
    await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['explain', 'yes', 'no']), state=RootStates.wait3)
async def callback_query2(query, state):
    if query.data == 'explain':
        text = 'Большинство наших бессмысленных мыслей касаются трех базовых инстинктов. Подумай о своей задаче хорошенько, может она не стоит того, чтобы тратить на нее время:'
        time.sleep(0.7)
        await bot.send_message(query.from_user.id, text)
        text = 'С самосохранением связаны наши тревоги о возможных событиях, которые мы не в силах сознательно и целенаправленно предотвратить.'
        time.sleep(0.5)
        await bot.send_message(query.from_user.id, text)
        text = 'С иерархическим инстинктом связаны переживания по поводу наших достижений, уважают нас или нет, сделаем мы что-то "великое" или нет и т. п.'
        time.sleep(0.6)
        await bot.send_message(query.from_user.id, text)
        text = 'С половым инстинктом связаны наши ожидания "идеального партнера", "настоящей любви", "всеобщего восхищения". Отсюда и переживания, что партнер "не тот", или его нет, или что все вокруг глупцы и не видят нашего совершенства.'
        time.sleep(0.8)
        await bot.send_message(query.from_user.id, text, reply_markup=limited_keyboard)

    elif query.data == 'yes':
        await RootStates.rs_answr6.set()
        text = 'Отлично! Ты определился со своими реальными мотивами и понял, что эта ситуация стоит того, чтобы быть обдуманной.'
        time.sleep(0.6)
        await bot.send_message(query.from_user.id, text)
        text = 'Как ты теперь  сформулируешь для себя вопрос, который мы будем решать? Чего мы хотим добиться?'
        time.sleep(0.5)
        await bot.send_message(query.from_user.id, text, reply_markup=explain_keyboard)

    elif query.data == 'no':
        await RootStates.rs_answr7.set()
        text = 'Если ты не можешь повлиять на ситуацию, стоит ли обдумывать этот вопрос дальше?'
        await bot.send_message(query.from_user.id, text, reply_markup=kill_keyboard)


@dp.callback_query_handler(Text(equals=['reset', 'stop']), state=RootStates.wait3)
async def joy_division(query, state):
    if query.data == 'reset':
        await reset(query, state)
    elif query.data == 'stop':
        await end1(query, state)


@dp.message_handler(state=RootStates.rs_answr5)
async def sex_pistols(msg, state):
    await state.update_data(rs_answr5=msg.text)
    await RootStates.wait3.set()
    text = 'Это очень здорово, что ты осознал избыточность прилагаемых усилий и решил остановиться на том, что уже есть. У тебя появился ресурс, чтобы решать по-настоящему важные задачи.'
    await msg.answer(text, reply_markup=another_question_stop_keyboard)


@dp.callback_query_handler(Text(equals=['explain', 'yes', 'no']), state=RootStates.wait2)
async def callback_query(query, state):
    if query.data == 'explain':
        text = 'Люди часто думают следующим образом: а вдруг еще одна порция усилий окажется небесполезной? Вдруг одиннадцатый информатор сможет дать нам гораздо лучший совет, чем предыдущие 10?'
        time.sleep(0.7)
        await bot.send_message(query.from_user.id, text)

        text = 'Это избыточное поведение по отношению к ситуации, в которой оказались люди, называется амплификацией.'
        time.sleep(0.5)
        await bot.send_message(query.from_user.id, text)

        text = 'Мы иногда убеждаем себя, что поступаем так, поскольку никогда нельзя быть уверенным в правильности своего решения.'
        time.sleep(0.7)
        await bot.send_message(query.from_user.id, text)

        text = 'Да, еще одно усилие действительно может оказаться решающим, но точно так же - и еще одно, и еще, и еще.'
        time.sleep(0.7)
        await bot.send_message(query.from_user.id, text)

        text = 'Следуя такой логике нужно консультироваться с каждым человеком на планете.'
        time.sleep(0.7)
        await bot.send_message(query.from_user.id, text, reply_markup=limited_keyboard)
    elif query.data == 'yes':
        text = 'На каком решении ты остановился?'
        await RootStates.rs_answr5.set()
        await bot.send_message(query.from_user.id, text)
    elif query.data == 'no':
        await RootStates.wait3.set()
        text = 'Важно понять насколько то, о чем ты думаешь, имеет смысл.'
        await bot.send_message(query.from_user.id, text)

        text = 'Можно ли в текущих обстоятельствах повлиять на решение ситуации?'

        tmp_keyboard = InlineKeyboardMarkup()

        for button in explain_keyboard.inline_keyboard:
            tmp_keyboard.add(*button)

        for button in limited_keyboard.inline_keyboard:
            tmp_keyboard.add(*button)

        await bot.send_message(query.from_user.id, text, reply_markup=tmp_keyboard)


@dp.message_handler(state=RootStates.rs_answr4)
async def what_do_you_do(msg, state):
    await state.update_data(rs_answr4=msg.text)
    await RootStates.wait2.set()
    async with state.proxy() as data:
        text = f'А можно ли ограничиться тем, что ты уже сделал{"" if data["gender"] else "а"} для достижения твоих целей?'
        tmp_keyboard = InlineKeyboardMarkup()
        for button in explain_keyboard.inline_keyboard:
            tmp_keyboard.add(*button)

        for button in limited_keyboard.inline_keyboard:
            tmp_keyboard.add(*button)

        await msg.answer(text, reply_markup=tmp_keyboard)


@dp.callback_query_handler(Text(equals=['reset', 'stop']), state=RootStates.wait1)
async def reset_stop(query, state):
    if query.data == 'reset':
        await reset(query, state)
    elif query.data == 'stop':
        await stop(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=RootStates.wait1)
async def dolores_oriordan_one_love(query, state):
    if query.data == 'yes':
        text = 'А что ты уже сделал для решения этого вопроса?'
        await RootStates.rs_answr4.set()
        await bot.send_message(query.from_user.id, text)
    elif query.data == 'no':
        text = 'Это очень здорово, что ты осознал нецелесообразность решения этого вопроса, не каждый может себе в этом признаться. У тебя появился ресурс, чтобы решать по-настоящему важные задачи.'
        await bot.send_message(query.from_user.id, text, reply_markup=another_question_stop_keyboard)


@dp.callback_query_handler(Text(equals=['explain', 'yes', 'no']), state=RootStates.wait)
async def think_about_it_2(query, state):
    if query.data == 'explain':
        text = 'В психологии есть такой феномен, как "эффект оправдания усилий". Когда вы затрачиваете много усилий и времени на определенную задачу или человека, вам сложно отказаться от них.'
        await bot.send_message(query.from_user.id, text, reply_markup=reasons_keyboard)
    elif query.data == 'yes':
        await RootStates.wait1.set()
        text = 'Понимая это, ты все еще хочешь продумать вопрос дальше?'
        await bot.send_message(query.from_user.id, text, reply_markup=want_keyboard)
    elif query.data == 'no':
        async with state.proxy() as data:
            text = f'А что ты уже сделал{"" if data["gender"] else "а"} для решения этого вопроса?'
            await RootStates.rs_answr4.set()
            await bot.send_message(query.from_user.id, text)


@dp.message_handler(state=RootStates.rs_answr3)
async def think_about_it(msg, state):
    await state.update_data(rs_answr3=msg.text)
    await RootStates.wait.set()
    async with state.proxy() as data:
        text = f"Подумай, может быть ты хочешь решить эту задачу только потому, что уже вложил{'' if data['gender'] else 'а'} в нее много сил и времени?"
        time.sleep(0.5)
        tmp_keyboard = InlineKeyboardMarkup()
        for button in explain_keyboard.inline_keyboard:
            tmp_keyboard.add(*button)

        for button in reasons_keyboard.inline_keyboard:
            tmp_keyboard.add(*button)

        await msg.answer(text, reply_markup=tmp_keyboard)


@dp.callback_query_handler(Text(equals=['explain']), state=RootStates.rs_answr3)
async def issue_clar(query, state):
    text = "Важно понять, каковы твои реальные мотивы в данной ситуации. Например, за формулировкой \"хочу быть в форме\" может стоять мотив \"нравиться противоположному полу\". Решения в этих двух случаях могут оказаться разные. Попробуй 5 раз задать себе вопрос \"Зачем мне нужно решить этот вопрос?\" и последовательно запиши все пять ответов в строку ввода."
    await bot.send_message(query.from_user.id, text)


@dp.message_handler(state=RootStates.rs_answr2)
async def has_you_idea(msg, state):
    await RootStates.rs_answr3.set()
    await state.update_data(rs_answr2=msg.text)
    text = "А почему это так важно для тебя?"
    time.sleep(0.5)
    await msg.answer(text, reply_markup=explain_keyboard)


@dp.message_handler(state=RootStates.rs_answr1)
async def resolve_issue(msg, state):
    await state.update_data(rs_answr1=msg.text)
    text = "Почему тебя беспокоит этот вопрос?"
    time.sleep(0.5)
    await msg.answer(text)
    await RootStates.rs_answr2.set()


@dp.callback_query_handler(Text(equals='get_example'), state=StartStates.gender)
async def get_example(query, state):
    text = "Я не успеваю завершить проект в срок, не знаю что делать."
    async with state.proxy() as data:
        if data.get('gender') is None:
            await bot.send_message(query.from_user.id, text, reply_markup=another_example_keyboard)
        elif data['gender']:
            await bot.send_message(query.from_user.id, text, reply_markup=male_another_example_keyboard)
        else:
            await bot.send_message(query.from_user.id, text, reply_markup=female_another_example_keyboard)


@dp.callback_query_handler(Text(equals='get_another_example'), state=StartStates.gender)
async def get_another_example(query, state):
    text = "Не знаю куда отправить ребенка учиться."
    async with state.proxy() as data:
        if data.get('gender') is None:
            await bot.send_message(query.from_user.id, text, reply_markup=yet_another_example_keyboard)
        elif data['gender']:
            await bot.send_message(query.from_user.id, text, reply_markup=male_yet_another_example_keyboard)
        else:
            await bot.send_message(query.from_user.id, text, reply_markup=female_yet_another_example_keyboard)


@dp.callback_query_handler(Text(equals='get_yet_another_example'), state=StartStates.gender)
async def get_yet_another_example(query, state):
    text = "Не могу решить менять ли мне машину."
    time.sleep(0.5)
    async with state.proxy() as data:
        if data.get('gender') is None:
            await bot.send_message(query.from_user.id, text, reply_markup=final_keyboard)
        elif data['gender']:
            await bot.send_message(query.from_user.id, text, reply_markup=male_final_keyboard)
        else:
            await bot.send_message(query.from_user.id, text, reply_markup=female_final_keyboard)


@dp.callback_query_handler(Text(equals='explain'), state=RootStates.rs_answr1)
async def clarif_quest(query, state):
    text = "Не волнуйся насчет точности формулировок в этом вопросе, просто передай смысл."
    await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['male', 'female']), state=StartStates.gender)
async def start_question(query, state):
    await state.update_data(gender=1 if query.data == 'male' else 0)
    async with state.proxy() as data:
        user_id = md5(get_name(query.from_user).encode()).hexdigest()

        user_find = users.find_one({'id': user_id})
        if user_find:
            async with state.proxy() as data:
                users.update({'_id': user_find['_id']},
                             {'$set': { 'name': user_find['name'],
                              'gender': data['gender']}})
    await start(query, state)


async def start(query, state):
    text = "Что мы обсудим?"
    time.sleep(0.5)
    await bot.send_message(query.from_user.id, text, reply_markup=explain_keyboard)
    await RootStates.rs_answr1.set()


@dp.callback_query_handler(Text(equals=['yes', 'no', 'talk', 'end']), state=StartStates.start_conv)
async def start_conv(query, state):
    if query.data  == 'yes':
        await StartStates.gender.set()
        text = "Отлично! Давай сразу обговорим детали. Пожалуйста, воспринимай меня, как тренажер."
        time.sleep(1)
        await bot.send_message(query.from_user.id, text)
        text = "Я не волшебник и не смогу дать готовых решений проблем за то время, что мы проведем вместе."
        time.sleep(2)
        await bot.send_message(query.from_user.id, text)
        text = "Однако, я могу напрячь твои серые клеточки так, что ты самостоятельно найдешь правильный ответ."
        time.sleep(2)
        await bot.send_message(query.from_user.id, text)
        text = "Для этого придется приложить усилия. Я буду задавать важные вопросы, а ты постарайся отвечать на них максимально подробно."
        time.sleep(1)
        await bot.send_message(query.from_user.id, text)
        text = "Ты можешь прерваться в любой момент и продолжить, когда будет удобно."
        time.sleep(0.7)
        await bot.send_message(query.from_user.id, text)
        text = "Приступим?"
        async with state.proxy() as data:
            if data.get('gender') is None:
                time.sleep(0.5)
                await bot.send_message(query.from_user.id, text, reply_markup=example_keyboard)
            else:
                if data['gender']:
                    time.sleep(0.5)
                    await bot.send_message(query.from_user.id, text, reply_markup=male_example_keyboard)
                else:
                    time.sleep(0.5)
                    await bot.send_message(query.from_user.id, text, reply_markup=female_example_keyboard)

    if query.data == 'talk':
        await MentalGumStates.paper.set()
        text = "Тебе будет комфортнее работать с мыслями на бумаге или здесь?"
        vstal_keyboard = InlineKeyboardMarkup()
        vstal_keyboard.add(*[InlineKeyboardButton(text="На бумаге", callback_data='paper'),
                            InlineKeyboardButton(text='Здесь', callback_data='here')])
        await bot.send_message(query.from_user.id, text, reply_markup=vstal_keyboard)
    if query.data == "no":
        text = "Если ты пока не знаешь что именно со мной обсудить, мы можем разобраться  с мыслями, которые крутятся в твоей голове. Так у нас получится выделить вопросы, которые требуют отдельного продумывания со мной."
        await bot.send_message(query.from_user.id, text, reply_markup=talk_problem_keyboard)
    if query.data == "end":
        await stop(query, state)


@dp.message_handler(state=StartStates.name)
async def process_name(msg, state):
    async with state.proxy() as data:
        data['name'] = msg.text
        text = f"Приятно познакомиться, {data['name']}!"
        user_id = md5(get_name(msg.from_user).encode()).hexdigest()

        users.insert_one({'id': user_id, 'name': msg.text})
        await msg.answer(text)
        await want_you_talk(msg, state)

async def want_you_talk(msg, state):
    await StartStates.start_conv.set()
    text = "У тебя есть конкретный вопрос, который ты хочешь со мной решить?"
    await bot.send_message(msg.from_user.id, text, reply_markup=situation_keyboard)
