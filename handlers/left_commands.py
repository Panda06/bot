import time

from aiogram.dispatcher.filters import Command, Text
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.types.input_file import InputFile

from handlers.commands import dp, bot, collection, reset
from handlers.right_commands import which_res, answer_has_no_idea
from handlers.root_commands import end
from handlers.states import HasIdea, HasOneIdea, HasSomeIdeas, RootStates
from handlers.keyboards import talk_keyboard, satisfied_keyboard, situation_keyboard, ready_button
from handlers.keyboards import new_task_keyboard, explain_keyboard, next_or_finish_keyboard, cont_talk_keyboard
from handlers.keyboards import satisfied_keyboard1, end_or_cont_keyboard, reset_or_stop_keyboard


@dp.callback_query_handler(Text(equals='cont_talk'), state=HasIdea.wait6)
async def replay(query, state):
    await HasIdea.wait3.set()
    await continue_(query, state)


@dp.message_handler(state=HasIdea.hi_answr17)
async def finish_left(msg, state):
    await state.update_data(hi_answr17=msg.text)
    text = 'Отлично! Теперь внеси эту задачу в календарь и нажми на кнопку "продолжить беседу", когда справишься с ней.'
    await HasIdea.wait6.set()
    await msg.answer(text, reply_markup=cont_talk_keyboard)


async def another_facts(query, state):
    text = 'В таком случае без дополнительных фактов о ситуации не обойтись.'
    await HasIdea.hi_answr17.set()
    await bot.send_message(query.from_user.id, text)
    async with state.proxy() as data:
        text = f'Сколько времени тебе нужно, чтобы поработать с этими источниками: {data["hi_answr11"]} ? Напиши дату и время, когда мы сможем продолжить разговор.'
        time.sleep(0.8)
        await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['reset', 'issue']), state=HasIdea.wait5)
async def resume(query, state):
    if query.data == 'reset':
        await reset(query, state)
    elif query.data == 'issue':
        await another_facts(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasIdea.wait4)
async def inaction(query, state):
    if query.data == 'yes':
        text = 'Отлично, если ты согласен с последствиями своего решения и понимаешь, что несешь за них ответственность, то мы можем обсудить другой вопрос?'
        await HasIdea.wait5.set()
        await bot.send_message(query.from_user.id, text, reply_markup=end_or_cont_keyboard)
    elif query.data == 'no':
        await another_facts(query, state)


@dp.message_handler(state=HasIdea.hi_answr16)
async def feels(msg, state):
    await state.update_data(hi_answr16=msg.text)
    await HasIdea.wait4.set()
    text = 'Ты согласен с  этими последствиями своего бездействия?'
    await msg.answer(text, reply_markup=situation_keyboard)


@dp.message_handler(state=HasIdea.hi_answr15)
async def feels(msg, state):
    await state.update_data(hi_answr15=msg.text)
    await HasIdea.hi_answr16.set()
    text = 'Как ты себя будешь чувствовать, если ты будешь оттягивать принятие решения?'
    await msg.answer(text)


@dp.message_handler(state=HasIdea.hi_answr14)
async def facts(msg, state):
    await state.update_data(hi_answr14=msg.text)
    await HasIdea.hi_answr15.set()
    text = 'Как твое решение не собирать факты отразится на людях, которые вовлечены в ситуацию?'
    await msg.answer(text)


@dp.message_handler(state=HasIdea.hi_answr13)
async def is_finish(msg, state):
    await state.update_data(hi_answr13=msg.text)
    await end(msg, state)


@dp.callback_query_handler(Text(equals=['reset', 'stop']), state=HasIdea.wait7)
async def smash_into_pieces(query, state):
    if query.data == 'reset':
        await reset(query, state)
    elif query.data == 'stop':
        await end1(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasIdea.wait7)
async def arctic_monkeys(query, state):
    if query.data == 'yes':
        text = 'Отлично, если ты согласен с последствиями своего решения и понимаешь, что несешь за них ответственность, то мы можем обсудить другой вопрос?'
        await bot.send_message(query.from_user.id, text, reply_markup=reset_or_stop_keyboard)
    elif query.data == 'no':
        await another_facts(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasIdea.wait3)
async def another_ideas(query, state):
    if query.data == 'yes':
        await HasIdea.hi_answr13.set()
        async with state.proxy() as data:
            ideas = data['hoi_answr1'] if data.get('hoi_answr1') else data['hsi_answr1']
            if data.get('hoi_answr1', None):
                text = f"{data['name']}, вначале нашего разговора, ты {'говорил' if data.get('gender', True) else 'говорила'}, что у тебя есть такая идея: {data['hoi_answr1']}"
                time.sleep(0.8)
                await bot.send_message(query.from_user.id, text)
                text = f"Представь, что ее нельзя реализовать. Что бы ты {'делал' if data.get('gender', True) else 'делала'} в этом случае?"
                time.sleep(0.7)
                await bot.send_message(query.from_user.id, text)
    elif query.data == 'no':
        async with state.proxy() as data:
            if data.get('hi_answr14'):
                text = 'В прошлый раз мы говорили про последствия твоего решения не собирать факты, ты согласен с ними?'
                await HasIdea.wait7.set()
                await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)
            else:
                text = 'А что произойдет в твоей жизни, если ты ничего не предпримешь?'
                await HasIdea.hi_answr14.set()
                await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['cont_talk']), state=HasIdea.wait3)
async def continue_(query, state):
    text = 'Привет! Ты успел поработать с источниками?'
    await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)


@dp.message_handler(state=HasIdea.hi_answr12)
async def datetime(msg, state):
    await HasIdea.wait3.set()
    await state.update_data(hi_answr12=msg.text)
    text = 'Отлично! Теперь внеси эту задачу в календарь и нажми на кнопку "продолжить беседу", когда справишься с ней.'
    await msg.answer(text, reply_markup=cont_talk_keyboard)


@dp.message_handler(state=HasIdea.hi_answr11)
async def sources(msg, state):
    await state.update_data(hi_answr11=msg.text)
    await HasIdea.hi_answr12.set()
    text = 'А сколько времени тебе нужно, чтобы поработать с ними? Напиши дату и время, когда мы сможем продолжить беседу.'
    await msg.answer(text)


@dp.callback_query_handler(Text(equals='ok'), state=HasIdea.wait2)
async def see_all(query, state):
    text = 'Отлично! К каким 2-3 источникам ты обратишься?'
    await HasIdea.hi_answr11.set()
    await bot.send_message(query.from_user.id, text)


@dp.message_handler(state=HasIdea.hi_answr10)
async def questions(msg, state):
    await HasIdea.wait2.set()
    await state.update_data(hi_answr10=msg.text)
    text = 'Отлично! Сейчас мы с тобой запланируем работу с открытыми источниками.'
    time.sleep(0.6)
    await msg.answer(text)
    text = "Они позволят тебе привнести новые факты в твое видение ситуации. У меня есть для тебя несколько вариантов. Посмотри!"
    time.sleep(0.6)
    await bot.send_message(msg.from_user.id, text)
    button = InlineKeyboardMarkup()
    async with state.proxy() as data:
        text = "Посмотрел" if data.get('gender', True) else "Посмотрела"
        button.add(InlineKeyboardButton(text=text, callback_data="ok"))
        await bot.send_photo(msg.from_user.id, InputFile("image.png"), reply_markup=button)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasIdea.wait)
async def can_anyone_help_you(query, state):
    if query.data == 'yes':
        await HasIdea.hi_answr10.set()
        text = "Поддержка других людей - наш самый важный ресурс. Позволь людям, которым ты доверяешь, помочь тебе."
        time.sleep(0.7)
        await bot.send_message(query.from_user.id, text)
        async with state.proxy() as data:
            text = f"Подумай кто в твоем окружении готов помочь тебе. Какие вопросы ты бы им задал{'' if data.get('gender', True) else 'а'} и какие задачи поручил{'' if data.get('gender', True) else 'а'}?"
            time.sleep(0.8)
        await bot.send_message(query.from_user.id, text)
    elif query.data == 'no':
        await HasIdea.wait2.set()
        text = "Ничего страшного, будем справляться самостоятельно! Сейчас мы с тобой запланируем работу с открытыми источниками."
        time.sleep(0.8)
        await bot.send_message(query.from_user.id, text)
        text = "Они позволят тебе привнести новые факты в твое видение ситуации. У меня есть для тебя несколько вариантов. Посмотри!"
        time.sleep(0.6)
        await bot.send_message(query.from_user.id, text)
        button = InlineKeyboardMarkup()
        async with state.proxy() as data:
            text = "Посмотрел" if data.get('gender', True) else "Посмотрела"
            button.add(InlineKeyboardButton(text=text, callback_data="ok"))
            await bot.send_photo(query.from_user.id, InputFile("image.png"), reply_markup=button)


@dp.callback_query_handler(Text(equals=['next', 'end']), state=HasIdea.wait)
async def next_or_end(query, state):
    if query.data == 'next':
        text = 'Скажи, ты можешь попросить кого-то помочь со сложными для тебя шагами?'
        await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)
    elif query.data == 'end':
        await end(query, state)


@dp.message_handler(state=HasIdea.hi_answr9)
async def faster(msg, state):
    await state.update_data(hi_answr9=msg.text)
    text = 'Тебе уже удалось принять решение?'
    await HasIdea.wait.set()
    await msg.answer(text, reply_markup=next_or_finish_keyboard)


@dp.message_handler(state=HasIdea.hi_answr8)
async def step(msg, state):
    await state.update_data(hi_answr8=msg.text)
    text = 'Может быть, выбранный  шаг тоже можно разделить на несколько более мелких задач. Запиши, что ты можешь сделать уже сегодня?'
    await HasIdea.hi_answr9.set()
    await msg.answer(text)


@dp.message_handler(state=HasIdea.hi_answr7)
async def faster(msg, state):
    await state.update_data(hi_answr7=msg.text) # ОС
    text = 'С каким из тех шагов, что ты выписал, ты справишься лучше и быстрее всего?'
    await HasIdea.hi_answr8.set()
    await msg.answer(text)


@dp.message_handler(state=HasIdea.hi_answr6)
async def issues(msg, state):
    await state.update_data(hi_answr6=msg.text)
    text = 'Попробуй подумать "в действиях". На какие 3-5 мелких задач можно разложить твою ситуацию?'
    await HasIdea.hi_answr7.set()
    await msg.answer(text)


@dp.message_handler(state=HasIdea.hi_answr5)
async def recources(msg, state):
    await state.update_data(hi_answr5=msg.text)
    text = 'Какие из этих ресурсов у тебя уже есть?'
    await HasIdea.hi_answr6.set()
    await msg.answer(text)


@dp.message_handler(state=HasIdea.hi_answr4)
async def need_recources(msg, state):
    await state.update_data(hi_answr4=msg.text)
    text = 'Какие ресурсы необходимы, чтобы достичь нужного результата?'
    await HasIdea.hi_answr5.set()
    await msg.answer(text)


@dp.message_handler(state=HasIdea.hi_answr3)
async def change(msg, state):
    await state.update_data(hi_answr3=msg.text)
    text = 'Как изменилось твое понимание ситуации после предыдущего вопроса?'
    await HasIdea.hi_answr4.set()
    await msg.answer(text)


@dp.message_handler(state=HasIdea.hi_answr2)
async def respect(msg, state):
    await state.update_data(hi_answr2=msg.text)
    text = 'Подумай о человеке, которого ты уважаешь больше всего. Что бы он сделал, если бы столкнулся с подобной ситуацией?'
    await HasIdea.hi_answr3.set()
    await msg.answer(text)


@dp.callback_query_handler(Text(equals=['explain']), state=HasIdea.hi_answr2)
async def explain_bad_option(query, state):
    text = 'Какие слабые стороны есть у твоей аргументации? В чем ты можешь ошибаться? Почему твое представление о ситуации может быть неверным?'
    await bot.send_message(query.from_user.id, text)


@dp.message_handler(state=HasIdea.hi_answr1)
async def bad_option(msg, state):
    await state.update_data(hi_answr1=msg.text)
    text = 'Почему этот вариант может оказаться неудачным?'
    await HasIdea.hi_answr2.set()
    await msg.answer(text, reply_markup=explain_keyboard)


@dp.message_handler(state=HasOneIdea.hoi_answr2)
async def option(msg, state):
    await state.update_data(hoi_answr2=msg.text)
    text = 'А зачем ты тогда держишься за этот вариант? Что в нем хорошего?'
    await HasIdea.hi_answr1.set()
    await msg.answer(text)

@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasOneIdea.wait3)
async def new_task_f(query, state):
    if query.data == 'yes':
        await reset(query, state)
    if query.data == 'no':
        await end(query, state)


@dp.callback_query_handler(Text(equals=['ready']), state=HasOneIdea.wait2)
async def ready(query, state):
    text = 'Обсудим другую задачу?'
    await HasOneIdea.wait3.set()
    await bot.send_message(query.from_user.id, text, reply_markup=new_task_keyboard1)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasOneIdea.wait2)
async def plan(query, state):
    if query.data == 'yes':
        text = 'Отлично! Вноси это в календарь, а я пока подожду здесь.'
        await bot.send_message(query.from_user.id, text, reply_markup=ready_button)
    elif query.data == 'no':
        text = 'Почему ты сомневаешься в этом варианте?'
        await HasOneIdea.hoi_answr2.set()
        await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['yes', 'no', 'effect']), state=HasOneIdea.wait)
async def satisfied(query, state):
    if query.data == 'yes':
        text = 'Можно ли прямо сейчас запланировать его и реализовать в ближайшее время?'
        await HasOneIdea.wait2.set()
        await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)
    elif query.data == 'no':
        await which_res(query, state)
    elif query.data == 'effect':
        await answer_has_no_idea(query, state)


@dp.message_handler(state=HasSomeIdeas.hsi_answr3)
async def nickelback(msg, state):
    await state.update_data(hsi_answr3=msg.text)
    text = 'А зачем ты тогда держишься за этот вариант? Что в нем хорошего?'
    await HasIdea.hi_answr1.set()
    await msg.answer(text)


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=HasSomeIdeas.wait4)
async def dance_monkey(query, state):
    if query.data == 'yes':
        await reset(query, state)
    if query.data == 'no':
        await end(query, state)


@dp.callback_query_handler(Text(equals=['yes', 'no', 'ready']), state=HasSomeIdeas.wait3)
async def one_republic(query, state):
    if query.data == 'yes':
        text = 'Отлично! Вноси это в календарь, а я пока подожду здесь.'
        await bot.send_message(query.from_user.id, text, reply_markup=ready_button)
    elif query.data == 'no':
        text = 'Почему он не устраивает тебя полностью?'
        await HasSomeIdeas.hsi_answr3.set()
        await bot.send_message(query.from_user.id, text)
    elif query.data == 'ready':
        text = 'Обсудим другую задачу?'
        await HasSomeIdeas.wait4.set()
        await bot.send_message(query.from_user.id, text, reply_markup=new_task_keyboard)


@dp.message_handler(state=HasSomeIdeas.hsi_answr2)
async def imagine_dragons(msg, state):
    await state.update_data(hsi_answr2=msg.text)
    await HasSomeIdeas.wait3.set()
    text = 'Можно ли прямо сейчас запланировать его и реализовать в ближайшее время?'
    await msg.answer(text, reply_markup=situation_keyboard)


@dp.callback_query_handler(Text(equals=['has', 'no_has', 'effects']), state=HasSomeIdeas.wait)
async def scorpions(query, state):
    if query.data == 'has':
        text = 'Какой из них делает это лучше всего?'
        await HasSomeIdeas.hsi_answr2.set()
        await bot.send_message(query.from_user.id, text)
    elif query.data in ['effects', 'no_has']:
        await answer_has_no_idea(query, state)


@dp.message_handler(state=HasSomeIdeas.hsi_answr1)
async def imagine_dragons(msg, state):
    await state.update_data(hsi_answr1=msg.text)
    await HasSomeIdeas.wait.set()
    async with state.proxy() as data:
        text = f'До этого мы выясняли, что тебе нужно решить этот вопрос, чтобы {data["rs_answr3"]}'
        time.sleep(0.7)
        await msg.answer(text)
        text = 'Есть ли среди твоих вариантов те, что удовлетворяют эту потребность?'
        time.sleep(0.4)
        await msg.answer(text, reply_markup=satisfied_keyboard1)


@dp.message_handler(state=HasOneIdea.hoi_answr1)
async def your_idea(msg, state):
    await state.update_data(hoi_answr1=msg.text)
    await HasOneIdea.wait.set()
    async with state.proxy() as data:
        text = f'До этого мы выясняли, что тебе нужно решить этот вопрос, чтобы {data["rs_answr3"]}'
        time.sleep(0.7)
        await msg.answer(text)
        text = 'Удовлетворяет ли этот вариант твою потребность?'
        time.sleep(0.4)
        await msg.answer(text, reply_markup=satisfied_keyboard)


@dp.callback_query_handler(Text(equals='has_one_idea'), state=RootStates.has_idea)
async def one_idea(query, state):
    await bot.send_message(query.from_user.id, "Отлично! Расскажи о ней.")
    await HasOneIdea.hoi_answr1.set()


@dp.callback_query_handler(Text(equals='has_some_idea'), state=RootStates.has_idea)
async def one_idea(query, state):
    await bot.send_message(query.from_user.id, "Отлично! Расскажи о них.")
    await HasSomeIdeas.hsi_answr1.set()


@dp.callback_query_handler(Text(equals=['has_idea']), state=RootStates.wait4)
async def answer_has_idea(query, state):
    await bot.send_message(query.from_user.id, "Сколько у тебя вариантов?",
                           reply_markup=talk_keyboard)
    await RootStates.has_idea.set()
