import time
from hashlib import md5

from aiogram.dispatcher.filters import Command, Text
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from handlers.commands import dp, bot, reset, stop, collection, users
from handlers.keyboards import ready_button, situation_keyboard
from handlers.keyboards import final_keyboard, male_final_keyboard, female_final_keyboard
from handlers.states import MentalGumStates
from utils import get_name


@dp.callback_query_handler(Text(equals=['yes', 'no']), state=MentalGumStates.wait8)
async def end_mental(query, state):
    if query.data == 'yes':
        async with state.proxy() as data:
            if data['paper']:
                await reset(query, state, dump=False)
            else:
                await reset(query, state)
    elif query.data == 'no':
        text = 'Ну хорошо, как появится желание решить какой-нибудь вопрос, нажми /start в меню. Я объясню детали.'
        async with state.proxy() as data:
            if not data['paper']:
                data_dump = data.as_dict()
                collection.insert_one(data_dump)
        await state.finish()
        await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals='ready'), state=MentalGumStates.wait6)
async def minds(query, state):
    text = 'Отлично, мы отфильтровали мысли в твоей голове. Теперь у тебя есть конкретные вопросы, которые ты можешь со мной решить.'
    await MentalGumStates.wait8.set()
    time.sleep(1)
    await bot.send_message(query.from_user.id, text)
    text = 'Хочешь обсудить со мной что-нибудь из твоих важных мыслей?'
    time.sleep(0.6)
    await bot.send_message(query.from_user.id, text, reply_markup=situation_keyboard)

@dp.message_handler(state=MentalGumStates.mg_answr4)
async def minds(msg, state):
    text = 'Отлично, мы избавились от хлама в твоей голове. Теперь у тебя есть конкретные вопросы, которые ты можешь со мной решить.'
    await MentalGumStates.wait8.set()
    await state.update_data(mg_answr4=msg.text)
    time.sleep(1)
    await msg.answer(text)
    text = 'Хочешь обсудить со мной что-нибудь из твоих важных мыслей?'
    time.sleep(0.6)
    await msg.answer(text, reply_markup=situation_keyboard)


@dp.callback_query_handler(Text(equals='ready'), state=MentalGumStates.wait6)
async def delete(query, state):
    text = 'Итак, в списке мыслей остались только важные вопросы, которые тебе нужно хорошенько продумать.'
    async with state.proxy() as data:
        if data['paper']:
            await MentalGumStates.wait7.set()
            await bot.send_message(query.from_user.id, text, reply_markup=ready)
        else:
            await MentalGumStates.mg_answr4.set()
            await bot.send_message(query.from_user.id, text)
            await bot.send_message(query.from_user.id, "Выпиши их.")


@dp.message_handler(state=MentalGumStates.mg_answr3)
async def inanity2(msg, state):
    await state.update_data(mg_answr3=msg.text)
    await MentalGumStates.wait6.set()
    text = 'Супер! Теперь продумай бессмысленность каждой из них и удали предыдущее сообщение.'
    await msg.answer(text, reply_markup=ready_button)


@dp.callback_query_handler(Text(equals='ready'), state=MentalGumStates.wait5)
async def inanity(query, state):
    text = 'Супер! Теперь продумай бессмысленность каждой из них и вычеркни из списка.'
    await MentalGumStates.wait6.set()
    await bot.send_message(query.from_user.id, text, reply_markup=ready_button)


@dp.callback_query_handler(Text(equals='ready'), state=MentalGumStates.wait4)
async def write(query, state):
    text = 'Отлично! Мы справились с важной задачей. Теперь давай поработаем с оставшейся частью списка.'
    time.sleep(0.6)
    await bot.send_message(query.from_user.id, text)

    text = 'Все эти мысли нужно будет разделить на две группы: \n- бессмысленные \n- требующие осмысления'
    time.sleep(0.6)
    await bot.send_message(query.from_user.id, text)

    text = 'К бессмысленным мыслям относятся любые суждения предельно общего характера - о мировой несправедливости, жестокости мира, твоей неудачливости и грезах о миллионе долларов.'
    time.sleep(1)
    await bot.send_message(query.from_user.id, text)

    text = 'Запиши их.'
    time.sleep(0.4)
    async with state.proxy() as data:
        if data['paper']:
            await MentalGumStates.wait5.set()
            await bot.send_message(query.from_user.id, text, reply_markup=ready_button)
        else:
            await MentalGumStates.mg_answr3.set()
            await bot.send_message(query.from_user.id, text)


@dp.message_handler(state=MentalGumStates.mg_answr2)
async def busy(msg, state):
    await state.update_data(mg_answr2=msg.text)
    await MentalGumStates.wait4.set()
    text = 'Теперь внеси все эти дела в свой календарь так, чтобы это было удобно и реалистично, а я пока подожду тебя тут.'
    time.sleep(0.8)
    await bot.send_message(msg.from_user.id, text, reply_markup=ready_button)


@dp.callback_query_handler(Text(equals='ready'), state=MentalGumStates.wait3)
async def busy(query, state):
    await MentalGumStates.wait4.set()
    text = 'Теперь внеси все эти дела в свой календарь так, чтобы это было удобно и реалистично, а я пока подожду тебя тут.'
    time.sleep(0.8)
    await bot.send_message(query.from_user.id, text, reply_markup=ready_button)


@dp.message_handler(state=MentalGumStates.mg_answr1)
async def gum_message(msg, state):
    await state.update_data(mg_answr1=msg.text)
    await MentalGumStates.mg_answr2.set()
    async with state.proxy() as data:
        text = f'Отлично! Теперь давай разделим этот список на две части:\n- то, что ты {"должен" if data["gender"] else "должна"} сделать\n- то, что ты {"должен" if data["gender"] else "должна"} продумать.'
        time.sleep(0.9)
        await bot.send_message(msg.from_user.id, text)
        text = f'Начнем с простого - списка дел, которые ты {"должен" if data["gender"] else "должна"} сделать, но так к ним и не приступишь: подготовить отчет, подстричь собаку, оплатить счета, купить батарейки и т.д.'
        time.sleep(0.8)
        await bot.send_message(msg.from_user.id, text)
        text = f'Какие пункты из списка ты можешь отнести к этой группе мыслей?'
        time.sleep(0.5)
        await bot.send_message(msg.from_user.id, text)


@dp.callback_query_handler(Text(equals='ready'), state=MentalGumStates.wait2)
async def gum(query, state):
    await MentalGumStates.wait3.set()
    async with state.proxy() as data:
        text = f'Отлично! Теперь давай разделим этот список на две части:\n- то, что ты {"должен" if data["gender"] else "должна"} сделать\n- то, что ты {"должен" if data["gender"] else "должна"} продумать.'
        time.sleep(0.9)
        await bot.send_message(query.from_user.id, text)
        text = f'Начнем с простого - списка дел, которые ты {"должен" if data["gender"] else "должна"} сделать, но так к ним и не приступишь: подготовить отчет, подстричь собаку, оплатить счета, купить батарейки и т.д.'
        time.sleep(0.8)
        await bot.send_message(query.from_user.id, text)
        text = f'Выпиши пункты из списка, которые  ты можешь отнести к этой группе мыслей.'
        time.sleep(0.5)
        await bot.send_message(query.from_user.id, text, reply_markup=ready_button)


@dp.callback_query_handler(Text(equals=['male', 'female']), state=MentalGumStates.wait1)
async def run(query, state):
    async with state.proxy() as data:
        if data.get('gender', None) is None:
            data['gender'] = query.data == 'male'

        user_id = md5(get_name(query.from_user).encode()).hexdigest()

        user_find = users.find_one({'id': user_id})
        if user_find:
            async with state.proxy() as data:
                users.update({'_id': user_find['_id']},
                             {'$set': { 'name': user_find['name'],
                              'gender': data['gender']}})
    async with state.proxy() as data:
        text = f'В твоей голове будут вспыхивать воспоминания:\n- люди, с которыми ты находишься в дискуссии; \n- ситуации, требующие твоего участия; \n- дела, которые ты {"должен" if data["gender"] else "должна"} сделать, но не знаешь как за них взяться или просто откладываешь.'

        time.sleep(1)
        await bot.send_message(query.from_user.id, text)
        text = 'Как только соответствующая мысль в тебе возникла, тут же записывай ее - и продолжай буждать в поисках следующей.'
        time.sleep(0.7)
        await bot.send_message(query.from_user.id, text)
        text = 'Например так:\n1) Нужно купить кроссовки\n2) Продолжать отношения с партнером или нет?\n3) Почему такая ужасная погода?\nи т.д.\nЗапиши.'
        time.sleep(0.8)
        if data['paper']:
            await MentalGumStates.wait2.set()
            await bot.send_message(query.from_user.id, text, reply_markup=ready_button)
        else:
            await MentalGumStates.mg_answr1.set()
            await bot.send_message(query.from_user.id, text)


@dp.callback_query_handler(Text(equals=['paper', 'here']), state=MentalGumStates.paper)
async def mental(query, state):
    await MentalGumStates.wait1.set()
    await state.update_data(paper=(query.data=='paper'))
    text = 'Перед тем как мы начнем разбираться в твоей проблеме, стоит выкинуть из головы весь сор, так ты сможешь думать яснее.'
    time.sleep(0.6)
    await bot.send_message(query.from_user.id, text)
    if query.data == 'here':
        text = 'Сядь поудобнее и позволь своим мыслям поблуждать.'
    else:
        text = 'Возьми ручку с бумагой, сядь поудобнее и позволь своим мыслям поблуждать.'

    time.sleep(0.5)
    async with state.proxy() as data:
        if data.get('gender') is None:
            await bot.send_message(query.from_user.id, text, reply_markup=final_keyboard)
        elif data['gender']:
            await bot.send_message(query.from_user.id, text, reply_markup=male_final_keyboard)
        else:
            await bot.send_message(query.from_user.id, text, reply_markup=female_final_keyboard)
