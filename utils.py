

def get_name(from_user):
    name = [str(from_user.id)]
    if from_user.first_name is not None:
        name.append(from_user.first_name)
    if from_user.username is not None:
        name.append(from_user.username)
    if from_user.last_name is not None:
        name.append(from_user.last_name)
    return "_".join(name)
