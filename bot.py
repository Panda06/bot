import datetime
import logging
import random
import time
import threading

from aiogram.utils.executor import start_polling
from collections import defaultdict

from utils import get_name
from handlers.misc import dp


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    start_polling(dp, skip_updates=True)
